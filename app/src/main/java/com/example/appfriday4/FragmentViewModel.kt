package com.example.appfriday4

import android.util.Log.d
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofitStuff.RetrofitService

class FragmentViewModel : ViewModel() {
    private val jsonLiveData = MutableLiveData<MutableList<MutableList<JsonModel>>>().apply {
        MutableLiveData<MutableList<MutableList<JsonModel>>>()
    }

    val _jsonLiveData = jsonLiveData

    private val importantsMap = MutableLiveData<MutableMap<Int, JsonModel>>().apply {
        MutableLiveData<MutableMap<Int, JsonModel>>()
    }

    val _importantsMap = importantsMap

    fun init() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                getJson()
            }
        }
    }


    private suspend fun getJson() {
        val result = RetrofitService.service().getJson()
        val data = result.body() as MutableList<MutableList<JsonModel>>

        jsonLiveData.postValue(result.body())


        val dataMap = mutableMapOf<Int, JsonModel>()
        data!!.forEach { it ->
            it.forEach {
                dataMap[it.field_id] = it
            }
        }

        importantsMap.postValue(dataMap)

    }
}