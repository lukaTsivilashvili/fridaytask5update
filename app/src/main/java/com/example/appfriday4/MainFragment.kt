package com.example.appfriday4

import android.os.Bundle
import android.util.Log.d
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appfriday4.databinding.MainFragmentBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainFragment : Fragment() {

    private val viewModel: FragmentViewModel by viewModels()
    private lateinit var binding: MainFragmentBinding
    private lateinit var myAdapter: RecyclerMainAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        init()

        return binding.root
    }

    private fun init() {
        viewModel.init()
        initRecycler()
        observe()
        lifecycleScope.launch {
            withContext(Dispatchers.Default) {
                initData()
            }
        }

    }


    private fun initRecycler() {
        myAdapter = RecyclerMainAdapter()
        binding.recycler.layoutManager = LinearLayoutManager(context)
        binding.recycler.adapter = myAdapter
    }

    private fun observe() {

        viewModel._jsonLiveData.observe(viewLifecycleOwner, {
            myAdapter.setData(it)
        })
    }

    private fun initData() {
        binding.regBtn.setOnClickListener {
            val infoMap: MutableMap<Int, String> = mutableMapOf()
            RecyclerChildAdapter.etInfoMap.forEach { editText ->
                val inputFieldModel = viewModel._importantsMap.value!![editText.key]!!
                if (inputFieldModel.required && (editText.value).isEmpty()) {
                    Toast.makeText(
                        requireContext(),
                        "Please Fill ${inputFieldModel.hint} Field",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@setOnClickListener
                } else {
                    infoMap[editText.key] = editText.value
                    d("map", infoMap.toString())
                }
            }

            Toast.makeText(
                requireContext(),
                "Successfully Added Info",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
