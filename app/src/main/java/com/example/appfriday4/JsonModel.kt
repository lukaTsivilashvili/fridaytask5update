package com.example.appfriday4

import com.google.gson.annotations.SerializedName

data class JsonModel(

    val field_id: Int,
    val hint: String,
    val field_type: String,
    val keyboard: String = "",
    val required: Boolean,
    val is_active: Boolean,
    val icon: String
)
