package com.example.appfriday4

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appfriday4.databinding.ActivityMainBinding
import com.example.appfriday4.databinding.MainFragmentBinding
import com.example.appfriday4.databinding.RecyclercardviewBinding

class RecyclerMainAdapter: RecyclerView.Adapter<RecyclerMainAdapter.MainViewHolder>() {
    val jsonList:MutableList<MutableList<JsonModel>> = mutableListOf()
    private val viewPool = RecyclerView.RecycledViewPool()

    inner class MainViewHolder(private val binding:RecyclercardviewBinding):RecyclerView.ViewHolder(binding.root){

        val recyclerView:RecyclerView = binding.childRecycler

    }

    fun setData(jsonList: MutableList<MutableList<JsonModel>>){
        this.jsonList.clear()
        this.jsonList.addAll(jsonList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val itemView =
            RecyclercardviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = MainViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {

        val childLayoutManager = LinearLayoutManager(holder.recyclerView.context)
        childLayoutManager.initialPrefetchItemCount = 4
        holder.recyclerView.apply {
            layoutManager = childLayoutManager
            adapter = RecyclerChildAdapter(jsonList[position])
            setRecycledViewPool(viewPool)
        }

    }

    override fun getItemCount() = jsonList.size

}