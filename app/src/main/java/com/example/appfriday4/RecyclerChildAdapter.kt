package com.example.appfriday4

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appfriday4.databinding.DatepickerLayoutBinding
import com.example.appfriday4.databinding.ItemMainRecyclerBinding
import com.example.appfriday4.databinding.RadioButtonLayoutBinding


class RecyclerChildAdapter(private val children: List<JsonModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {

        val etInfoMap: MutableMap<Int, String> = mutableMapOf()

        private const val VIEW_TYPE_INPUT = 1
        private const val VIEW_TYPE_DATE = 2
        private const val VIEW_TYPE_RADIO = 3
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            VIEW_TYPE_INPUT -> ViewHolder(
                ItemMainRecyclerBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

            VIEW_TYPE_DATE -> {
                DateViewHolder(
                    DatepickerLayoutBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            else -> {
                RadioViewHolder(
                    RadioButtonLayoutBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
        }


    }

    override fun getItemCount(): Int {
        return children.size
    }


    inner class ViewHolder(private val binding: ItemMainRecyclerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val childView = children[adapterPosition]


            if (etInfoMap[childView.field_id] == null) {
                etInfoMap[childView.field_id] = ""
                binding.root.id = childView.field_id
                binding.root.hint = childView.hint
            } else {
                binding.root.setText(etInfoMap[childView.field_id])
                binding.root.id = childView.field_id
                binding.root.hint = childView.hint
            }
            when (childView.keyboard) {
                "text" -> binding.root.inputType = InputType.TYPE_CLASS_TEXT
                "number" -> binding.root.inputType = InputType.TYPE_CLASS_NUMBER
            }

            binding.root.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable?) {
                    etInfoMap[childView.field_id] = s.toString()
                }

            })
        }
    }


    inner class DateViewHolder(private val binding: DatepickerLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind() {

        }

    }

    inner class RadioViewHolder(private val binding: RadioButtonLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind() {

            val childView = children[adapterPosition]

            binding.root.id = childView.field_id

        }


    }


    override fun getItemViewType(position: Int): Int {
        when (children[position].hint) {
            "Birthday" -> return VIEW_TYPE_DATE
            "Gender" -> return VIEW_TYPE_RADIO
            else -> return VIEW_TYPE_INPUT
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> holder.onBind()
            is DateViewHolder -> holder.onBind()
            is RadioViewHolder -> holder.onBind()
        }
    }
}
